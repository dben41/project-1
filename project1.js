/**
 * Daryl Bennet
 * CS 4150 Spring 2015
 * Project 1
 *
 * This file contains all my functions necessary for completing the project.
 */


/*
 * Returns my name
 */
function GetName(){
	return "Daryl Bennett";
}

/*
 * Returns my UID
 */
function GetUNID(){
	return "u0699753";
}

/*
 * My implementation of Gradeschool add
 */
 function GradeSchoolAdd(x,y) {
 	var xIndex = x.length-1;
 	var yIndex = y.length-1;
 	var max = x.length;
 	if(y.length > max) max = y.length;
 	var carry = 0;
 	var result = "";
 	//Loop through the chars
 	for(var i = max; i > 0; i--, xIndex--, yIndex--)
 	{
 		//add all the ints
 		if(yIndex > -1 && xIndex > -1)	
 			var temp = parseInt(x[xIndex]) + parseInt(y[yIndex]) + carry;
 		else if(yIndex > -1)
 			var temp = parseInt(y[yIndex]) + carry;
 		else if(xIndex > -1)
 			var temp = parseInt(x[xIndex]) + carry;
 		else if(carry==1)
 			var temp = 1;
 			//reset
 			carry = 0;
 			//see if there's a carry over
 			if(temp > 9){
 				carry = 1;
 				temp = temp - 10;
 			}
 			//append to end of string
 			result = temp + result;

 		}
 		//a sloppy way to append last carry
 		//i could alternatively add a max+1 in for loop
 		if(carry==1) return "1" + result;	
 		return result;	
 	}
	
function GradeSchoolMultiply(x,y) {


	//check for 0
	if(x.replace(/^(0+)/g, '') == ""  || y.replace(/^(0+)/g, '')  == "") return "0";
	
	//do padding to length if incoming number length is odd
	if(x.length % 2 != 0 && x.length != 1) x = "0" + x;
	if(y.length % 2 != 0 && y.length != 1) y = "0" + y;

	//find the bigger length of the two
	var length = x.length;
	if(y.length > length) length = y.length;

	//check if length is 1 (base case)
	if(x.length == 1 && y.length == 1) {
		var xInt = parseInt(x);
		var yInt = parseInt(y);
		var temp12 = xInt * yInt;
		return temp12.toString();
	} else if (y.length > x.length) {
    //make the two ints the same length by padding
		var count = y.length-1;
		while(count > 0 && x.length != y.length){
			x = "0" + x;
			count--;
		}
	} else if (x.length > y.length ) {
     //make the two ints the same length by padding
		var count = x.length-1;
		while(count > 0 && x.length != y.length){
			y = "0" + y;
			count--;
		}
	}

	//if there's still ints to parse
	var halfIndex = Math.floor(length/2);
	var xLeft = x.substring(0, halfIndex);
	var xRight = x.substring(halfIndex, length);
	var yLeft = y.substring(0, halfIndex);
	var yRight = y.substring(halfIndex, length);


	//break up recursive calls for clarity and for debuggin'
	var op1 = GradeSchoolMultiply(xLeft,yLeft);	
	var op2 = GradeSchoolMultiply(xLeft, yRight);
	var op3 = GradeSchoolMultiply(xRight,yLeft);
	var op4 = GradeSchoolMultiply(xRight,yRight);

	//break it down for debuggingfor debuggin
	//add 0 strings
	//var power1 = powerOfTen(length);
	//var power2 = powerOfTen(halfIndex);

/*	var part1 = GradeSchoolMultiply(power1,op1);
	var part2 = GradeSchoolAdd(op2,op3);
	var part3 = GradeSchoolMultiply(power2, part2);
	var part4 = GradeSchoolAdd(part1, part3);
	var answer = IterativeMultiply(part4,op4); */

	var part1 = powerOfTen(op1, length);
	var part2 = GradeSchoolAdd(op2,op3);
	var part3 = powerOfTen(part2, halfIndex);
	var part4 = GradeSchoolAdd(part1, part3);
	var answer = GradeSchoolAdd(part4,op4);

	
	//get rid of leading 0's
	return answer.toString().replace(/^(0+)/g, '');

}

function IterativeMultiply(x,y){
	var answer = "";
	var tempAnswer = 0;
	var carry = 0;
	for(i = x.length-1; i >= 0; i--){
		for(j = y.length-1; j >= 0; j--){
			tempAnswer = parseInt(x[i]) * parseInt(y[j]);
			tempAnswer += carry;
			//reset
			carry = 0;

			if(tempAnswer > 9){
 				carry = Math.floor(tempAnswer/10);
 				tempAnswer = tempAnswer % 10;
 			}

 			answer = tempAnswer + answer;
 			//if this is the last iteration, append the carry
 			if(j>=0 && i>=0) answer = carry + answer;

		}
	}

	return answer.toString();
}

function powerOfTen(result, length)
{
	//initial
	result += "0";
	while(length-1>0){
		result += "0";
		//decrement
		length--;
	}
	return result;
}

/**
 * This is to peform the subtraction for karatsubas
 * We assume that x.length >= y.length always. We'll never return negative #'s'
 */
function BasicSubtract(x,y){
	var xIndex = x.length-1;
	var yIndex = y.length-1;
	var result = "";
	var endLoop = false;

	//start from the far rightmost int and work your way left
	while(xIndex > -1 && !endLoop) {
		//check to see if y is still positive
		if(yIndex > -1){
			var tempx = parseInt(x[xIndex]);
			var tempy = parseInt(y[yIndex]);
			if(tempy > tempx){
				//do awful crawling algorithm
				//assume we can add 10 already
				tempx += 10;
				var tempIndex = xIndex-1;
				for(tempIndex; parseInt(x[tempIndex]) == 0; tempIndex-- ){
					//replace the 0 as 9
					//x[tempIndex] = "9";
					x = setCharAt(x, tempIndex, '9');
				}
				var temp = parseInt(x[tempIndex])-1;
				//we've reached the non-zero value, decrement
				//x[tempIndex] = temp + "";
				x = setCharAt(x,tempIndex, temp.toString());
				
				//now perform actual subtraction
				result = Math.floor(tempx-tempy) + result;
			} else {
				//floor might not be necessary
				result = Math.floor(tempx-tempy) + result;
			}
		} else{
			//place remaining intsss
			result = x.substr(0, xIndex+1) + result;
			endLoop = true;
		}

		//decrement
		xIndex--;
		yIndex--;
	}

	//get rid of leading 0's
	return result.replace(/^(0+)/g, '');
}

/**
 * Implements Karatsuba's multiplication
 */
function KaratsubasMultiply(x,y) {
	//check for 0
	if(x.replace(/^(0+)/g, '') == ""  || y.replace(/^(0+)/g, '')  == "") return "0";

	//do padding to length if odd
	if(x.length % 2 != 0 && x.length != 1) x = "0" + x;
	if(y.length % 2 != 0 && y.length != 1) y = "0" + y;

	//find the bigger length of the two
	var length = x.length;
	if(y.length > length) length = y.length;

	//check if length is 1 (base case)
	if(x.length == 1 && y.length == 1) {
		var xInt = parseInt(x);
		var yInt = parseInt(y);
		var tempI = xInt * yInt;
		return tempI.toString();
	} else if (y.length > x.length) {
    //make the two ints the same length by padding
		var count = y.length-1;
		while(count > 0 && x.length != y.length){
			x = "0" + x;
			count--;
		}
	} else if (x.length > y.length ) {
     //make the two ints the same length by padding
		var count = x.length-1;
		while(count > 0 && x.length != y.length){
			y = "0" + y;
			count--;
		}
	}

	//if there's still ints to parse
	var halfIndex = Math.floor(length/2);
	var xLeft = x.substring(0, halfIndex);
	var xRight = x.substring(halfIndex, length);
	var yLeft = y.substring(0, halfIndex);
	var yRight = y.substring(halfIndex, length);

	var p1 = KaratsubasMultiply(xLeft,yLeft);
	var p2 = KaratsubasMultiply(xRight,yRight);
	var temp1 = GradeSchoolAdd(xLeft, xRight);
	var temp2 = GradeSchoolAdd(yLeft,yRight);
	var p3 = KaratsubasMultiply(temp1,temp2);

	//break it down
	var op1 = powerOfTen(p1, length);
	var op2 = BasicSubtract(p3, p1);
	var op3 = BasicSubtract(op2, p2);
	var op4 = powerOfTen(op3, halfIndex);
	var op5 = GradeSchoolAdd(op1,op4);
	var answer = GradeSchoolAdd(op5, p2);

	//get rid of leading 0's
	return answer.toString().replace(/^(0+)/g, '');
}

function BlendedMultiply(x,y,threshold)
{
	//check to see if the functions are within the threshold
	if(x.length < threshold && y.length < threshold)
		return GradeSchoolMultiply(x,y);

	//do the other algorithm
	//do padding to length if odd
	if(x.length % 2 != 0 && x.length != 1) x = "0" + x;
	if(y.length % 2 != 0 && y.length != 1) y = "0" + y;

	//find the bigger length of the two
	var length = x.length;
	if(y.length > length) length = y.length;

	//check if length is 1 (base case)
	if(x.length == 1 && y.length == 1) {
		var xInt = parseInt(x);
		var yInt = parseInt(y);
		var tempI = xInt * yInt;
		return tempI.toString();
	} else if (y.length > x.length) {
    //make the two ints the same length by padding
		var count = y.length-1;
		while(count > 0 && x.length != y.length){
			x = "0" + x;
			count--;
		}
	} else if (x.length > y.length ) {
     //make the two ints the same length by padding
		var count = x.length-1;
		while(count > 0 && x.length != y.length){
			y = "0" + y;
			count--;
		}
	}

	//if there's still ints to parse
	var halfIndex = Math.floor(length/2);
	var xLeft = x.substring(0, halfIndex);
	var xRight = x.substring(halfIndex, length);
	var yLeft = y.substring(0, halfIndex);
	var yRight = y.substring(halfIndex, length);

	var p1 = BlendedMultiply(xLeft,yLeft);
	var p2 = BlendedMultiply(xRight,yRight);
	var p3 = BlendedMultiply(GradeSchoolAdd(xLeft, xRight).toString(),GradeSchoolAdd(yLeft,yRight).toString());

	//break it down
	var op1 = powerOfTen(p1, length);
	var op2 = BasicSubtract(p3, p1);
	var op3 = BasicSubtract(op2, p2);
	var op4 = powerOfTen(op3, halfIndex);
	var op5 = GradeSchoolAdd(op1,op4);
	var answer = GradeSchoolAdd(op5, p2);

	//get rid of leading 0's
	return answer.toString().replace(/^(0+)/g, '');


}


/**
 * Helper fxn borrowed from StackOverflow
 * http://stackoverflow.com/questions/1431094/how-do-i-replace-a-character-at-a-particular-index-in-javascript#answer-1431110
 */
function setCharAt(str,index,chr) {
    if(index > str.length-1) return str;
    return str.substr(0,index) + chr + str.substr(index+1);
}

/**
 * This returns a constant
 *
 * The method of testing for this method is stupid and inaccurate. I change the threshold all over the map
 * and the time doesn't change at all.
 */
function GetOptimalBlendedThreshold() {
	return 150;
}